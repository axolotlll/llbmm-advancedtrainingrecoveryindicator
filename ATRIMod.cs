﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdvancedTrainingRecoveryIndicator;
using LLScreen;
using UnityEngine;

namespace AdvancedTrainingRecoveryIndicator
{
    class ATRIMod : MonoBehaviour
    {
        private static ATRIMod instance;
        public bool active = false;
        public ATRecoveryIndicator indicator = null;

        public static bool InGame => (UnityEngine.Object)World.instance != (UnityEngine.Object)null && (DNPFJHMAIBP.HHMOGKIMBNM() == JOFJHDJHJGI.CDOFDJMLGLO || DNPFJHMAIBP.HHMOGKIMBNM() == JOFJHDJHJGI.LGILIJKMKOD) && !UIScreen.loadingScreenActive;


        public static void Initialize()
        {
            GameObject target = new GameObject(nameof(ATRIMod));
            instance = target.AddComponent<ATRIMod>();
            UnityEngine.Object.DontDestroyOnLoad(target);

        }

        private void Update()
        {
            if (JOMBNFKIHIC.GIGAKBJGFDI.PNJOKAICMNN == GameMode.TRAINING && InGame)
            {
                if (!instance.active)
                {
                    Debug.Log("Setting ATRI active");
                        instance.active = true;
                        instance.indicator = World.instance.gameObject.AddComponent<ATRecoveryIndicator>();
                    
                    
                }
            }
            else
            {
                if (instance.active)
                {
                    Debug.Log("Setting ATRI inactive");
                    instance.active = false;
                    instance.indicator = null;
                }
            }
        }
    }
}
