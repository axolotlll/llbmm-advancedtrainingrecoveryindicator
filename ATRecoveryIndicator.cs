﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameplayEntities;
using LLHandlers;
using UnityEngine;

namespace AdvancedTrainingRecoveryIndicator
{
    public class ATRecoveryIndicator : MonoBehaviour
    {
        private PlayerEntity playerOne;
        private int flash;
        private AudioClip beep;
        private readonly int sampleFreq = 44000;
        private readonly float frequency = 1200f;
        private readonly float length = 0.05f;
        private float[] samples;
        private AudioSource audio;
        private void Start()
        {
            this.playerOne = PlayerHandler.instance.GetPlayerEntity(0);
            this.flash = 0;
            this.samples = new float[(int)(length * sampleFreq)];
            this.beep = AudioClip.Create("beeep", samples.Length, 1, sampleFreq, false);
            for (int i = 0; i < samples.Length; i++)
            {
                samples[i] = Mathf.Sin(Mathf.PI * 2 * i * frequency / sampleFreq);
            }
            this.beep.SetData(samples, 0);

            this.audio = gameObject.AddComponent<AudioSource>();
            this.audio.clip = this.beep;
            this.audio.priority = 0;
        }

        private void OnDestroy()
        {
            //this.playerOne.SetColorOutlines(false);
            //this.playerOne.GetVisual().ResetMaterials();
        }

        private void Update()
        {
            
            if (this.playerOne.abilityData.hitSomething)
            {

                this.playerOne.SetColorOutlines(true);
                if (this.playerOne.moveableData.velocity.CGJJEHPPOAN.EPOACNMBMMN <=
                    -this.playerOne.maxGravity.EPOACNMBMMN)
                    this.playerOne.SetColorOutlinesColor(Color.magenta);
                else this.playerOne.SetColorOutlinesColor(Color.black);
                this.flash = 8;
            }
            else
            {
                if (this.flash-- > 0)
                {
                    if (this.flash == 7)
                    {
                        //this.audio.Play();
                    }
                    this.playerOne.SetColorOutlines(true);
                    this.playerOne.SetColorOutlinesColor(Color.white);
                }
                else
                {
                    if (this.playerOne.moveableData.velocity.CGJJEHPPOAN.EPOACNMBMMN < 0L)
                    {
                        this.playerOne.SetColorOutlines(true);
                        if (this.playerOne.moveableData.velocity.CGJJEHPPOAN.EPOACNMBMMN <=
                            -this.playerOne.maxGravity.EPOACNMBMMN)
                            this.playerOne.SetColorOutlinesColor(Color.magenta);
                        else
                            this.playerOne.SetColorOutlines(false);
                    }
                    else
                        this.playerOne.SetColorOutlines(false);
                   
                }
            }
        }
    }
}
